# README #

This readme contains information useful for getting the taxonomy browser running.

### What does the taxonomy browser do?###

The taxonomy browser can open a taxdmp.zip file and retrieve all nodes within.
Information of these nodes can then be given to the user based on commanline input.

More information can be found in the JavaDocs located in /target/site/apidocs.

Current funcionality:

* -h/--help to display all commandline arguments.
* -s/--summary to display file info of the zip file.
* -z/--zip the path to taxdmp.zip (application will not work without this)
* -s/--summary to display file info of the zip file.
* -t/--tax_id displays information about organism with this ID number.
* -lc/--list_children lists all children of organism with given ID.
* -s/--sort used with -lc to sort the outpu based on (DEFAULT,TAX_ID, SCI_NAME, CHILD_NODES)
* -o/--omit_subspecies Omits subspecies when listing children.
* -fl/--fetch_lineage fetches the lineage of given organism from root the the organism itself.
* -l/--limit limits the lineage search to n nodes.

Currently missing functionality:

* Sorting children by rank.
* Retrieving all nodes of a given rank.
* Clear error handling for file IO and illegal arguments

Please be nice to it.


### How do I get set up? ###

This is a maven managed project which can be built with all its dependencies using the command 'mvn package'
in the project directory. The TaxonomyBrowser-1.0-SNAPSHOT.jar can then be run using the commands shown above.

As the taxdmp.zip is quite large it may be useful running java with extra memory by using the -Xmx2g command.

A first command to start with might be 'java -Xmx2g -jar TaxonomyBrowser-1.0-SNAPSHOT.jar -z [LOCATION OF TAXDMP.zip] -t 9606'
This will display taxonomic information about Homo Sapiens.


### Who do I talk to? ###

Any questions about the program or it's functioning can be directed at David van de Veerdonk
email: d.van.de.veerdonk@st.hanze.nl