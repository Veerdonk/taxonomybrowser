/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.io.IOException;

/**
 *
 * @author dvandeveerdonk
 */
public class TaxonomyBrowser {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        TaxonomyBrowser browser = new TaxonomyBrowser();
        browser.start(args);
    }
    /**
     *
     * @param args
     * @throws IOException
     */
    private void start(String[] args) throws IOException {
        CliController clicont = new CliController();
        clicont.useGnuParser(args);

    }
}
