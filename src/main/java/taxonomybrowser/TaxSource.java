/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.io.IOException;

/**
 *
 * @author dvandeveerdonk
 */
public interface TaxSource {

    /**
     *
     * @return
     * @throws IOException
     */
    TaxCollection readZip() throws IOException;
}
