/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.util.ArrayList;

/**
 *
 * @author dvandeveerdonk
 */
class TaxEntry {
    private final int tax_id;
    private final int parent_id;
    private final String rank;
    private String name;
    private ArrayList<Integer> children;

/**
 *
 *
 * @param tax_id
 * @param parent_id
 * @param rank
 * @param name
 */
    public TaxEntry(
    final int tax_id,
    final int parent_id,
    final String rank,
    final String name){
        this.name = "";
        this.tax_id = tax_id;
        this.parent_id = parent_id;
        this.rank = rank;
    }
/**
 * @param children
 */
    public void setChildren(ArrayList<Integer> children) {
    this.children = children;
    }
/**
 *
 * @return
 */
    public ArrayList<Integer> getChildren() {
        return children;
    }
/**
 *
 * @param name
 */
    public void setName(String name) {
        this.name = name;
    }
/**
 *
 * @return
 */
    public int getTax_id() {
        return tax_id;
    }
/**
 *
 * @return
 */
    public int getParent_id() {
        return parent_id;
    }
/**
 *
 * @return
 */
    public String getRank() {
        return rank;
    }

/**
 *
 */
    public void toStringPrint() {
        System.out.println("\nTax_id: \t" + this.getTax_id() + "\nname: \t"
                + this.getName() + "\nRank: \t" + this.getRank() + "\nParent: \t" + this.getParent_id());
    }
/**
 *
 * @return
 */
    public String getName() {
        return name;
    }
}
