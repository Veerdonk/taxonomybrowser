/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author dvandeveerdonk
 */
public class dmpReader implements TaxSource{

    private final String dataFile;

    /**
     *
     * @param dataFilePath
     */
    public dmpReader(final String dataFilePath){
        this.dataFile = dataFilePath;
    }

    @Override
    public TaxCollection readZip() throws IOException{
        File zipFile = new File(this.dataFile);
        ZipFile archive = new ZipFile(zipFile);
        ZipEntry nodesFile = archive.getEntry("nodes.dmp");
        ZipEntry namesFile = archive.getEntry("names.dmp");

        TaxCollection taxmap = new TaxCollection();
        TaxCollection taxMapByParent = new TaxCollection();

        try (InputStream is = archive.getInputStream(nodesFile)) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            int lineCount = 0;
            String line;
            while ((line = br.readLine()) != null) {
                lineCount++;
                String[] elements = line.split("\\t+\\|\\t+");
                int tax_id = Integer.parseInt(elements[0]);
                int parent = Integer.parseInt(elements[1]);
                String rank = elements[2];
                TaxEntry t = new TaxEntry(tax_id, parent, rank, "");
                TaxEntry tp = new TaxEntry(parent, tax_id, rank, "");
                taxmap.addTax(t);
            }
        is.close();
        }

        try(InputStream is = archive.getInputStream(namesFile)){
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            int lineCount = 0;
            String line;
            while ((line = br.readLine()) != null){
                lineCount ++;
                String[] elements = line.split("\\t+\\|\\t+");
                int tax_id = Integer.parseInt(elements[0]);
                String name = elements[1];
                TaxEntry val = taxmap.getTaxMap().get(tax_id);
                TaxEntry parent = taxmap.getTaxMap().get(val.getParent_id());

                ArrayList<Integer> kids = new ArrayList<Integer>();
                if(parent.getChildren() != null){
                    kids.addAll(parent.getChildren());
                    if(!kids.contains(val.getTax_id())){

                        kids.add(val.getTax_id());
                        parent.setChildren(kids);
                    }
                }
                else{
                    kids.add(val.getTax_id());
                    parent.setChildren(kids);
                }


                val.setName(name);
                taxmap.addTax(val);
                taxMapByParent.addTax(val);

            }
        }
    return taxmap;
    }
}
