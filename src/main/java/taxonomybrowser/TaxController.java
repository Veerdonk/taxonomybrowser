/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author dvandeveerdonk
 */
public class TaxController {
    /**
     *
     * @param data
     * @return
     * @throws java.io.IOException
     */
    public TaxCollection getTaxMap(String data) throws IOException {
        TaxSource dataSource = new dmpReader(data);
        TaxCollection taxMap = dataSource.readZip();
        return taxMap;
    }
    /**
     *
     * @param taxMap
     * @param path
     */
    public void summary_hm(TaxCollection taxMap, String path){
        File file = new File(path);
        System.out.println("\nFilename: \t\t" + file.getName() + "\nDownload date: \t\t"
                + new Date(file.lastModified()) + "\nnumber of nodes: \t" + taxMap.getNodes());
    }
    /**
     *
     * @param taxMap
     * @param tax_id
     */
    public void getTaxInfo(TaxCollection taxMap, int tax_id) {
        TaxEntry taxa = taxMap.getTax(tax_id);
        taxa.toStringPrint();
        System.out.println("\nPARENT INFO:");
        taxMap.getTax(taxa.getParent_id()).toStringPrint();
        System.out.println("CHILDREN:\n");
        for (int kid = 0; kid < taxa.getChildren().size(); kid++) {
            int curKid = taxa.getChildren().get(kid);
            System.out.println(kid + 1 + ": \t" + curKid + taxMap.getTax(curKid).getName());
        }
    }
    /**
     *
     * @param taxMap
     * @param tax_id
     * @param sorting
     * @param omit
     */
    public void listChildren(TaxCollection taxMap, int tax_id, String sorting, boolean omit) {

        TaxEntry startNode = taxMap.getTax(tax_id);

        ArrayList<TaxEntry> children = new ArrayList();
        ArrayList<TaxEntry> allKids = this.kids(taxMap, startNode, children);



        if (omit == true) {
            ArrayList<TaxEntry> omittedKids = new ArrayList();
            for (TaxEntry kid : allKids) {
                if (!"subspecies".equals(kid.getRank())) {
                    omittedKids.add(kid);
                }
            }
            allKids = omittedKids;
        }
        System.out.println("TAX_ID;PARENT_TAX_ID;RANK;SCIENTIFIC NAME;CHILD NUMBER");
        switch(sorting) {
            case "DEFAULT":
                System.out.println("\nNo sorting options received, sorting by tax_id.");
            case "TAX_ID":
                Collections.sort(allKids, new Comparator<TaxEntry>() {
                    @Override public int compare(TaxEntry t1, TaxEntry t2) {
                        return t1.getTax_id() - t2.getTax_id();
                    }
                });
                for (TaxEntry kid: allKids) {
                    if (kid.getChildren() != null) {
                       System.out.println(kid.getTax_id() + ";" + kid.getParent_id()
                               + ";" + kid.getRank() + ";" + kid.getName() + ";" + kid.getChildren().size());
                    }
                    System.out.println(kid.getTax_id() + ";" + kid.getParent_id()
                            + ";" + kid.getRank() + ";" + kid.getName() + ";0");
                }
                break;
            case "SCI_NAME":
                Collections.sort(allKids, new Comparator<TaxEntry>() {
                    @Override public int compare(TaxEntry t1, TaxEntry t2) {
                        return t1.getName().compareTo(t2.getName());
                    }
                });
                System.out.println(allKids);
                for (TaxEntry kid: allKids ){
                    if (kid.getChildren() != null) {
                       System.out.println(kid.getTax_id() + ";" + kid.getParent_id() + ";"
                               + kid.getRank() + ";" + kid.getName() + ";" + kid.getChildren().size());
                    }
                    System.out.println(kid.getTax_id() + ";" + kid.getParent_id()
                            + ";" + kid.getRank() + ";" + kid.getName() + ";0");
                }
                break;
            case "CHILD_NODES":
                Collections.sort(allKids, new Comparator<TaxEntry>() {
                    @Override public int compare(TaxEntry t1, TaxEntry t2){
                        if (t1.getChildren() != null && t2.getChildren() != null) {
                            return t1.getChildren().size() - t2.getChildren().size();
                        }
                        else if (t1.getChildren() == null && t2.getChildren() != null) {
                            return -1;
                        }
                        else if (t1.getChildren() != null && t2.getChildren() == null) {
                            return 1;
                        }
                        else{
                            return 0;
                        }

                    }
                });
                for (TaxEntry kid: allKids) {
                    if (kid.getChildren() != null) {
                       System.out.println(kid.getTax_id() + ";" + kid.getParent_id()
                              + ";" + kid.getRank() + ";" + kid.getName() + ";" + kid.getChildren().size());
                    }
                    System.out.println(kid.getTax_id() + ";" + kid.getParent_id()
                            + ";" + kid.getRank() + ";" + kid.getName() + ";0");
                }
                break;
        }
    }
    /**
     *
     * @param taxMap
     * @param tax
     * @param children
     * @return
     */
    public ArrayList kids(TaxCollection taxMap, TaxEntry tax, ArrayList children){

        if (tax.getChildren() != null) {
            for (Integer kidNr: tax.getChildren()) {
                TaxEntry kid = taxMap.getTax(kidNr);

                children.add(kid);
                this.kids(taxMap, taxMap.getTax(kidNr), children);
            }
        }
    return children;
    }
    /**
     *
     * @param taxMap
     * @param tax_id
     * @param limit
     */
    public void lineage(TaxCollection taxMap, int tax_id, int limit){

        ArrayList<TaxEntry> lineageList = new ArrayList();

        if (limit != 0){
            List<TaxEntry> lineageListLimit = this.getLineageList(taxMap, tax_id, new ArrayList<TaxEntry>()).subList(0, limit);
            lineageList = new ArrayList(lineageListLimit);
        }
        else{
            ArrayList<TaxEntry> lineageListUnlimit = this.getLineageList(taxMap, tax_id, new ArrayList<TaxEntry>());
        }

        Collections.reverse(lineageList);
        System.out.println("TAX_ID;SCI_NAME;RANK");
        for (TaxEntry node : lineageList) {
            System.out.println(node.getTax_id() + ";" + node.getName() + ";" + node.getRank());
        }


    }
    /**
     *
     * @param taxMap
     * @param tax_id
     * @param lineageList
     * @return
     */
    private ArrayList<TaxEntry> getLineageList(TaxCollection taxMap, int tax_id, ArrayList lineageList){
        TaxEntry node = taxMap.getTax(tax_id);
        if(node.getTax_id() != 1){
            lineageList.add(node);
            this.getLineageList(taxMap, node.getParent_id(), lineageList);
        }
        else if(node.getTax_id() == 1){
            lineageList.add(node);
        }
        return lineageList;
    }
}