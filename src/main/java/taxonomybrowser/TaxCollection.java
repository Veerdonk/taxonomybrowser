/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxonomybrowser;

import java.util.HashMap;

/**
 *
 * @author dvandeveerdonk
 */
class TaxCollection {
    private final HashMap<Integer, TaxEntry> taxMap = new HashMap();

    /**
     *
     * @param entry
     */
    public void addTax(TaxEntry entry){
        this.taxMap.put(entry.getTax_id(), entry);
    }

    /**
     *
     * @return
     */
    public HashMap<Integer, TaxEntry> getTaxMap() {
        return taxMap;
    }
    /**
     *
     * @return
     */
    public int getNodes(){
        return this.taxMap.size();
    }
    /**
     *
     * @param tax_id
     * @return
     */
    public TaxEntry getTax(int tax_id){
        TaxEntry tax = this.taxMap.get(tax_id);
        return tax;
    }
}
